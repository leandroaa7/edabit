/*
Test.assertEquals(mumbling("MubAshIr"), "M-Uu-Bbb-Aaaa-Sssss-Hhhhhh-Iiiiiii-Rrrrrrrr")
Test.assertEquals(mumbling("maTT"), "M-Aa-Ttt-Tttt")
Test.assertEquals(mumbling("airForce"), "A-Ii-Rrr-Ffff-Ooooo-Rrrrrr-Ccccccc-Eeeeeeee")
Test.assertEquals(mumbling("EdaBit"), "E-Dd-Aaa-Bbbb-Iiiii-Tttttt")
Test.assertEquals(mumbling("PaKiStAn"), "P-Aa-Kkk-Iiii-Sssss-Tttttt-Aaaaaaa-Nnnnnnnn")
 */

function mumbling(str) {
	let arr = [...str]
    arr = arr.map( (value,index) => (value.toUpperCase() + Array(index+1).join(value.toLowerCase())) )
    return arr.join("-")
}

/* 
alternativa 
function mumbling(str) {
	return [...str.toLowerCase()]
		.map((l, i) => l.toUpperCase() + l.repeat(i))
		.join('-');
}
 */