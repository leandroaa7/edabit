/* Test.assertEquals(missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]), 5)
Test.assertEquals(missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]), 10)
Test.assertEquals(missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]), 1)
Test.assertEquals(missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]), 7)
Test.assertEquals(missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9]), 3)
 */


function missingNum(arr) {
    const numbers = [...Array(10).keys()].map(el => el + 1);
    const totalNumbers = numbers.reduce((total, number) => { return total + number }, 0)
    const totalArr = arr.reduce((total, number) => { return total + number }, 0);
    return totalNumbers - totalArr;
}

console.log(missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]))