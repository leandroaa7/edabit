/* Test.assertSimilar(removeDups(['John', 'Taylor', 'John']), ['John', 'Taylor'])
Test.assertSimilar(removeDups(['John', 'Taylor', 'John', 'john']), ['John', 'Taylor', 'john'])
Test.assertSimilar(removeDups(['javascript', 'python', 'python', 'ruby', 'javascript', 'c', 'ruby']), ['javascript', 'python', 'ruby', 'c'])
Test.assertSimilar(removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5]), [1, 2, 3, 5, 6, 7])
Test.assertSimilar(removeDups(['#', '#', '%', '&', '#', '$', '&']), ['#', '%', '&', '$'])
Test.assertSimilar(removeDups([3, 'Apple', 3, 'Orange', 'Apple']), [3, 'Apple', 'Orange'])
 */

function removeDups(arr) {
    const newArr = [];

    for (const element of arr) {
        if(!newArr.includes(element)){
            newArr.push(element);
        }
    }

    return newArr;
}

console.log(removeDups(['#', '#', '%', '&', '#', '$', '&']))