/**
 * @param {Array<String>} string 
 */
function reverseWords(string) {
    return string
        .trim()
        .split(' ')
        .reverse()
        .join(' ');
}

console.log(reverseWords("hello world!"))