
/* 
Test.assertEquals(oppositeHouse(1, 3), 6)
Test.assertEquals(oppositeHouse(3, 3), 4)
Test.assertEquals(oppositeHouse(2, 3), 5)
Test.assertEquals(oppositeHouse(3, 5), 8)
Test.assertEquals(oppositeHouse(7, 11), 16)
Test.assertEquals(oppositeHouse(10, 22), 35)
Test.assertEquals(oppositeHouse(20, 3400), 6781)
Test.assertEquals(oppositeHouse(9, 26), 44)
Test.assertEquals(oppositeHouse(20, 10), 1) 
Test.assertEquals(oppositeHouse(23633656673, 310027696726), 596421736780)
*/

function oppositeHouse(house, n) {
        const maxValue = (n * 2) + 1;
        return (maxValue - house)
}