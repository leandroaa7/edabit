/* 
Test.assertSimilar(findLetters("monopoly"), ["m", "n", "p", "l", "y"])
Test.assertSimilar(findLetters("balloon"), ["b", "a", "n"])
Test.assertSimilar(findLetters("analysis"),  ["n", "l", "y", "i"])
Test.assertSimilar(findLetters("summer"), ["s", "u", "e", "r"])
Test.assertSimilar(findLetters("apple"), ["a", "l", "e"])
Test.assertSimilar(findLetters("commission"), ["c", "n"])
Test.assertSimilar(findLetters("fox"), ["f", "o", "x"])

*/

function findLetters(str) {
    const arr = [...str], map1 = {}, map2 = {}, invalid = -1;
    let index = 0;

    for (const char of arr) {
        const isCharAdded = map1[char] !== undefined
        if (!isCharAdded) {
            map1[char] = index
            map2[index] = char
            index++
        } else if (map1[char] !== invalid) {
            const charIndex = map1[char]
            map1[char] = invalid

            const nextIndex = charIndex + 1
            const nextChar = map2[nextIndex];
            const hasNext = map2[nextIndex] !== undefined

            if (hasNext) {
                map2[charIndex] = map2[nextIndex]
                delete map2[nextIndex]
                map1[nextChar] = charIndex
                index = nextIndex;
            } else {
                delete map2[charIndex]
            }
        }
    }
    return Object.values(map2);
}

/* 
com regex
  [...txt].filter(v => txt.match(new RegExp(`${v}`, "g")).length === 1);


com indexOf = aqui compara se a ultima ocorrência é igual a primeira, logo so tem uma
	return arr.filter((e,i) => arr.indexOf(e) == arr.lastIndexOf(e));

    
*/

