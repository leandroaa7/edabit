/* Test.assertEquals(isAnagram("cristian", "Cristina"), true)
Test.assertEquals(isAnagram("Dave Barry", "Ray Adverb"), true)
Test.assertEquals(isAnagram("Nope", "Note"), false)
Test.assertEquals(isAnagram("Apple", "Appeal"), false) */


function isAnagram(s1, s2) {
    s1 = s1.split('').map(element => element.toLowerCase()).sort()
    s2 = s2.split('').map(element => element.toLowerCase()).sort()
    const isDifferentSize = s1.length !== s2.length;
    if (isDifferentSize) { return false }
    for (let i = 0; i < s1.length; i++) {
        if (s1[i] !== s2[i]) {
            return false;
        }
    }
    return true;
}

console.log(isAnagram("cristian", "Cristina"));