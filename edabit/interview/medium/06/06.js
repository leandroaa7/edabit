/*
Test.assertEquals(nameScore('MUBASHIR'), "THE BEST")
Test.assertEquals(nameScore('MATT'), "THE BEST")
Test.assertEquals(nameScore('PAKISTAN'), "THE BEST")
Test.assertEquals(nameScore('AIRFORCE'), "THE BEST")
Test.assertEquals(nameScore('GUV'), 'NOT TOO GOOD')
Test.assertEquals(nameScore('PUBG'),"NOT TOO GOOD")
Test.assertEquals(nameScore('ME'), "PRETTY GOOD")
Test.assertEquals(nameScore('BOB'),"PRETTY GOOD")
Test.assertEquals(nameScore('JLJ'), 'PRETTY GOOD')
Test.assertEquals(nameScore('YOU'), 'VERY GOOD')
Test.assertEquals(nameScore('FABIO'),"VERY GOOD")
Test.assertEquals(nameScore('ROBBY'), 'THE BEST')
Test.assertEquals(nameScore('BILL GATES'), "THE BEST")
*/

const scores = {"A": 100, "B": 14, "C": 9, "D": 28, "E": 145, "F": 12, "G": 3,
"H": 10, "I": 200, "J": 100, "K": 114, "L": 100, "M": 25,
"N": 450, "O": 80, "P": 2, "Q": 12, "R": 400, "S": 113, "T": 405,
"U": 11, "V": 10, "W": 10, "X": 3, "Y": 210, "Z": 23};

function nameScore(name) {
    const initialValue = 0;
    let result = "";
	let score = name
        .trim()
        .replace(" ", "")
        .split('')
        .reduce((previousValue, currentValue) => previousValue + scores[currentValue], initialValue )
    
    // para transformar string em array pode ser [...name]

    if (score <= 60){
        result =  "NOT TOO GOOD"
    } else if (score >= 61 && score <= 300){
        result =  "PRETTY GOOD"
    } else if (score >= 301 && score <= 599){
        result =  "VERY GOOD"
    }else {
        result = "THE BEST"
    }
    return result;
}

/**
 * other solution
function nameScore(name) {
	const score = [...name].reduce((a, c) => a + scores[c], 0);
	if (score <= 60)  return "NOT TOO GOOD";
	if (score <= 300) return "PRETTY GOOD";
	if (score <= 600) return "VERY GOOD";
	return "THE BEST";
}
 */