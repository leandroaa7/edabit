/**
Test.assertEquals(findASeat(20, [3, 5, 4, 2]), 3)
Test.assertEquals(findASeat(1000, [50, 20, 80, 90, 100, 60, 30, 50, 80, 60]), 0)
Test.assertEquals(findASeat(200, [35, 23, 40, 21, 38]), -1)
Test.assertEquals(findASeat(200, [35, 23, 18, 10, 40]), 2)
Test.assertEquals(findASeat(21, [6, 3, 7]), 1)
Test.assertEquals(findASeat(11037, [1839, 0, 0]), 0)
 */

function findASeat(n, arr) {
	const maxCapacity = n / arr.length, percent = 100;
    return arr.findIndex(carriage => {
        const capacityInPercent = ((carriage * percent) / maxCapacity) 
        return capacityInPercent <= 50
    })
}

// outra alternativa seria usar dentro de um while