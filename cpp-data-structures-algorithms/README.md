# [Data structures and algorithms course ](https://www.udemy.com/course/cpp-data-structures-algorithms-levelup-prateek-narang/)

## Configure c++
- [oficial doc](https://code.visualstudio.com/docs/cpp/config-clang-mac) 
- [debug with same version](https://stackoverflow.com/questions/49397233/how-to-enable-c17-support-in-vscode-c-extension)
- [configure debug](https://code.visualstudio.com/docs/cpp/config-clang-mac#_customize-debugging-with-launchjson)
- [Makefile Tools December: Problem Matchers and Compilation Database Generation](https://devblogs.microsoft.com/cppblog/makefile-tools-december-2021-update-problem-matchers-and-compilation-database-generation/)
- [sonarLint](https://github.com/SonarSource/sonarlint-vscode/wiki/C-and-CPP-Analysis)
- [configure sonalint with vscode](https://hackmd.io/@aeefs2Y8TMms-cjTDX4cfw/SkWgTfus5)

### Need to input value or string into console ?
change `externalConsole` to `true` into **.vscode/launch.json**
## Commands

1. `g++ test.cpp -o output.bin`
2. `./output.bin`

## Theory

### Iterator
- É o mecanismo usado para "andar", elemento por elemento, por uma coleção de dados. É uma forma abstrata e genérica de tratar o avanço entre os elementos dessa coleção. Esse avanço pode se dar de várias formas, inclusive ao contrário.
- O funcionamento exato depende de cada tipo de dado, o importante é que se um tipo possui um iterador em conformidade com a linguagem toda operação que iteração poderá ser feita com aquele objeto. Não importa para ele a complexidade da operação, nem como ela deverá ser feita. É uma forma independente da implementação de acessar os dados da coleção.
- Ele possui os métodos begin() e end() pra indicar onde começa e onde termina a iteração.
- Os tipos de iterator são, na ordem de menos completo para o mais completo
  - [Input iterators](https://www.geeksforgeeks.org/random-access-iterators-in-cpp/#:~:text=Library%2C%20others%20being%3A-,Input%20iterators,-Output%20iterator)
  - [Output iterator](https://www.geeksforgeeks.org/random-access-iterators-in-cpp/#:~:text=Input%20iterators-,Output%20iterator,-Forward%20iterator%C2%A0)
  - [Forward iterator ](https://www.geeksforgeeks.org/random-access-iterators-in-cpp/#:~:text=Output%20iterator-,Forward%20iterator%C2%A0,-Bidirectional%20iterator)
  - [Bidirectional iterator](https://www.geeksforgeeks.org/bidirectional-iterators-in-cpp/)
  - [Random Access Iterators](https://www.geeksforgeeks.org/random-access-iterators-in-cpp/)
  - O Random Access Iterator é o mais completo
- [Link](https://pt.stackoverflow.com/questions/164232/o-que-%C3%A9-iterator)

#### Begin() and End()
-   os comandos begin() e end() retornam respectivamente um ponteiro para o inicio e para o fim da lista, a diferença é que o ponteiro do **begin** aponta realmente para o **primeiro elemento** da lista enquanto o **end** aponta para **depois do ultimo elemento**.
- begin() returns an iterator referring to the first element in the container. end() returns an iterator which is the past-the-end value for the container. If the container is empty, then begin() == end();
- [If the container is empty, this function returns the same as vector::begin.](https://cplusplus.com/reference/vector/vector/end/)
### unordered_set

- find('item')
  - [The unordered_set::find() function is used to search for an element in the container. It returns an iterator to the element, if found else, it returns an iterator pointing to unordered_set::end()](https://www.geeksforgeeks.org/unordered_set-find-function-in-c-stl/)
- end()
  - [The C++ unordered_set::end function returns the iterator pointing to the past-the-last element of the unordered_set container](https://www.alphacodingskills.com/cpp/notes/cpp-unordered-set-end.php#:~:text=The%20C%2B%2B%20unordered_set%3A%3Aend,hence%20could%20not%20be%20dereferenced.)

### auto
- A partir do C++11 serve para declarar variáveis cujo tipo vai ser inferido pelo compilador a partir da inicialização delas.
-  Uma vez atribuído um valor para uma variável auto, é impossível mudar o tipo da variável
-  [link](https://pt.stackoverflow.com/questions/5775/quando-usar-void-e-auto)

### cin : (lê-se C IN)
- Existe o cin (c in) e o cout (c out)
- `cin` is the standard input stream. Usually the stuff someone types in with a keyboard. We can extract values of this stream, using the >> operator. So `cin >> n;` reads an integer.
- `cin >> n >> g` is equivalent to `cin >> n; cin >> g;` So we now know that cin >> n >> g takes two integer values from the input stream and returns a reference to cin.

#### >> operator or extraction operator
- [link 1](https://cplusplus.com/reference/istream/istream/operator%3E%3E/)
- [link 2](https://en.cppreference.com/w/cpp/io/basic_istream/operator_gtgt)

example 
```
#include <iostream>     // std::cin, std::cout, std::hex

int main () {
  int n;

  std::cout << "Enter a number: ";
  std::cin >> n;
  std::cout << "You have entered: " << n << '\n';

  return 0;
}
```

#### cin.get()
cin.get() is used for accessing character array. It includes white space characters. Generally, cin with an extraction operator (>>) terminates when whitespace is found. However, cin.get() **reads a string with the whitespace**.
Syntax:

`cin.get(string_name, size);`

example 

```
#include <iostream>
using namespace std;
  
int main()
{
    char name[25];
    cin.get(name, 25);
    cout << name;
  
    return 0;
}
```

**Input:**

`Geeks for Geeks`
**Output:**

`Geeks for Geeks`

example 2

```
#include <iostream>
using namespace std;
  
int main()
{
    char name[100];
    cin.get(name, 3);
    cout << name;
  
    return 0;
}
```

**Input:**
`GFG`
**Output:**
`GF`

- [link](https://www.geeksforgeeks.org/cin-get-in-c-with-examples/)

### static
- static variable in a **function**
  - it gets allocated for the lifetime of the program
  - Even if the function is called multiple times, space for the static variable is allocated only once and the value of variable in the previous call gets carried through the next function call.
  - example 
```
#include <iostream>
#include <string>
using namespace std;
void demo()
{
    // static variable
    static int count = 0;
    cout << count << " ";
    // value is updated and will be carried to next function calls
    count++;
}
int main()
{
    for (int i=0; i<5; i++)    
        demo();
    return 0;
}

// output -> 0 1 2 3 4 

```

- [link](https://www.geeksforgeeks.org/static-keyword-cpp/)

### Pointers and reference

#### Pointers 
Example:
```
// C++ program to illustrate Pointers
#include <bits/stdc++.h>
using namespace std;
void geeks()
{
    int var = 20;
    // declare pointer variable
    int* ptr;
    // note that data type of ptr and var must be same
    ptr = &var;
    // assign the address of a variable to a pointer
    cout << "Value at ptr = " << ptr << "\n";
    cout << "Value at var = " << var << "\n";
    cout << "Value at *ptr = " << *ptr << "\n";
}
// Driver program
int main() 
{ 
  geeks(); 
  return 0;
}
// output
/**
Value at ptr = 0x7ffe454c08cc
Value at var = 20
Value at *ptr = 20
*/
```

example 2
```
#include <iostream>
using namespace std;

int main() {
   int x = 5; // declara uma variável inteira x e atribui o valor 5 a ela
   int *ptr; // declara um ponteiro para inteiro

   ptr = &x; // atribui o endereço de memória de x ao ponteiro ptr

   cout << "O valor de x é: " << x << endl;
   cout << "O endereço de memória de x é: " << &x << endl;
   cout << "\n" << endl;
   
   cout << "O valor do ponteiro ptr é: " << ptr << endl;
   cout << "O endereço de memória apontado pelo ponteiro ptr é: " << ptr << endl;
   cout << "\n" << endl;
   
   cout << "O endereço de memória do ponteiro ptr é: " << &ptr << endl;
   cout << "O valor apontado pelo ponteiro ptr é: " << *ptr << endl;
   cout << "\n" << endl;

   *ptr = 10; // modifica o valor apontado pelo ponteiro ptr

   cout << "O novo valor de x é: " << x << endl;

   return 0;
}
```

- [link cpp](https://www.geeksforgeeks.org/cpp-pointers/)
- [link c](https://www.geeksforgeeks.org/c-pointers/)


#### operador de desferência *
O operador de desreferência em C++ é representado pelo símbolo * (asterisco) e é usado para acessar o valor apontado por um ponteiro. Por exemplo, se p é um ponteiro para um objeto do tipo int, então *p acessa o valor armazenado nesse objeto.

#### References &
When a variable is declared as a reference, it becomes an alternative name for an existing variable. A variable can be declared as a reference by putting ‘&’ in the declaration.

Declaration of a Reference variable is preceded with the ‘&’ symbol ( but do not read it as “address of”).

-  it is convenient to think of a reference as another name for the same variable.
- [link](https://www.geeksforgeeks.org/references-in-c/)
- [link 2](https://stackoverflow.com/questions/57483/what-are-the-differences-between-a-pointer-variable-and-a-reference-variable)

o simbolo & pode significar:
-  **operador de endereço**, que é usado para obter o endereço de uma variável.
   -  se x é uma variável do tipo int, então &x retorna o endereço de x na memória. O resultado da expressão &x é um ponteiro do tipo int *, que pode ser usado para acessar ou modificar o valor de x.
-  **operador de referência**, que é usado para criar uma referência a uma variável. Uma referência é um "apelido" para uma variável existente, que pode ser usada como um nome alternativo para essa variável
   - se x é uma variável do tipo int, então int &ref = x; cria uma referência chamada ref para a variável x. A partir desse momento, ref pode ser usada como um nome alternativo para x

**outro código**
```
int x = 10;
int& ref = x; // cria uma referência para a variável x
```

#### referência de um ponteiro
exemplo 
```
int x = 10;
int* p = &x; // ponteiro para x
int*& ref = p; // referência a um ponteiro
```

Nesse exemplo, a variável ref é uma referência para um ponteiro que aponta para a variável x. Qualquer alteração feita na referência ref será refletida no ponteiro p. Por exemplo, se atribuirmos nullptr à referência ref, o ponteiro p também apontará para nullptr:

```
ref = nullptr;
std::cout << p << '\n'; // saída: 0
```

Por outro lado, se atribuirmos o endereço de outra variável à referência ref, o ponteiro p passará a apontar para essa nova variável:

```
int y = 20;
ref = &y;
std::cout << *p << '\n'; // saída: 20

```


#### Reference performance example
In For Each Loop to modify all objects: We can use references in for each loop to modify all elements. 
```
#include <bits/stdc++.h>
using namespace std;
 
int main()
{
    vector<int> vect{ 10, 20, 30, 40 };
 
    // We can modify elements if we
    // use reference
    for (int& x : vect) {
        x = x + 5;
    }
 
    // Printing elements
    for (int x : vect) {
        cout << x << " ";
    }
    cout << '\n';
 
    return 0;
}
```

We can use references in each loop to avoid a copy of individual objects when objects are large. 

other example 

```

#include <bits/stdc++.h>
using namespace std;
 
int main()
{
    vector<string> vect{ "geeksforgeeks practice",
                         "geeksforgeeks write",
                         "geeksforgeeks ide" };
 
    // We avoid copy of the whole string
    // object by using reference.
    for (const auto& x : vect) {
        cout << x << '\n';
    }
 
    return 0;
}
```

#### Passing By Pointer  - call by pointer
Here, the memory location (address) of the variables is passed to the parameters in the function, and then the operations are performed

example 
```
#include <iostream>
using namespace std;
  
void swap(int *x, int *y)
{
    int z = *x;
    *x = *y;
    *y = z;
}
  
int main()
{
    int a = 45, b = 35;
    cout << "Before Swap\n";
    cout << "a = " << a << " b = " << b << "\n";
    swap(&a, &b);
    cout << "After Swap with pass by pointer\n";
    cout << "a = " << a << " b = " << b << "\n";
}
```

**output**
```
Before Swap
a = 45 b = 35
After Swap with pass by pointer
a = 35 b = 45
```
[link](https://www.geeksforgeeks.org/passing-by-pointer-vs-passing-by-reference-in-cpp/)

#### Passing By Reference - Call by Reference 
It allows a function to modify a variable without having to create a copy of it. We have to declare reference variables. The memory location of the passed variable and parameter is the same and therefore, any change to the parameter reflects in the variable as well.

```
#include <iostream>
using namespace std;
void swap(int& x, int& y)
{
    int z = x;
    x = y;
    y = z;
}
  
int main()
{
    int a = 45, b = 35;
    cout << "Before Swap\n";
    cout << "a = " << a << " b = " << b << "\n";
  
    swap(a, b);
  
    cout << "After Swap with pass by reference\n";
    cout << "a = " << a << " b = " << b << "\n";
}
```

**output**
```
Before Swap
a = 45 b = 35
After Swap with pass by reference
a = 35 b = 45
```


- [link](https://www.geeksforgeeks.org/passing-by-pointer-vs-passing-by-reference-in-cpp/)

**exemplo 2** 

```
void dobrar(int& num) {
    num *= 2;
}

int main() {
    int x = 5;
    dobrar(x); // passando x por referência
    cout << x << endl; // exibe 10
    return 0;
}
```

usamos dobrar(x) em vez de dobrar(&x) porque a função dobrar espera um argumento do tipo int&, ou seja, uma referência a uma variável do tipo int. Quando passamos x como argumento, o compilador automaticamente passa uma referência à variável x, que é o que a função espera.

Se usássemos dobrar(&x) em vez de dobrar(x), estaríamos passando um ponteiro para o endereço de x, que é do tipo int*. Nesse caso, teríamos que modificar a assinatura da função dobrar para receber um ponteiro do tipo int* ao invés de uma referência do tipo int&. Então, o código ficaria assim:

```
void dobrar(int* num) {
    *num *= 2;
}

int main() {
    int x = 5;
    dobrar(&x); // passando o endereço de x
    cout << x << endl; // exibe 10
    return 0;
}
```

Nesse caso, precisamos usar o operador de desreferência * na função dobrar para acessar o valor da variável apontada pelo ponteiro num.





#### Referência para um ponteiro *&
- Uma referência é uma variável que é usada para se referir a outra variável
- um ponteiro é uma variável que armazena o endereço de memória de outra variável.
- Juntos, uma referência para um ponteiro é uma variável que pode ser usada para se referir a um ponteiro, em vez de se referir diretamente ao valor armazenado no endereço apontado pelo ponteiro
- A referência para um ponteiro pode ser útil em situações em que você precisa passar um ponteiro para uma função ou método e deseja que a função ou método possa alterar o valor do ponteiro.

exemplo
```
int x = 10;
int* p = &x;
int*& rp = p;
```

Aqui, p é um ponteiro para x. A linha int*& rp = p declara uma referência rp para um ponteiro para int. A referência rp pode ser usada para se referir ao ponteiro p, em vez de se referir diretamente ao valor armazenado no endereço apontado por p.

**explicação 2**
Suponha que você tenha um ponteiro chamado `ponteiro1` que aponta para um inteiro, e você quer passá-lo para uma função para que a função possa alterar o valor apontado por esse ponteiro.

Você pode passar o ponteiro para a função usando uma referência para um ponteiro `*&`. A referência permite que a função modifique o valor apontado pelo ponteiro original. Por exemplo:
```
void alteraPonteiro(int* &ptr) {
    *ptr = 42; // Modifica o valor apontado pelo ponteiro
}

int main() {
    int valor = 10;
    int* ponteiro1 = &valor;

    alteraPonteiro(ponteiro1);

    std::cout << *ponteiro1 << std::endl; // Imprime "42"
    return 0;
}
```

Neste exemplo, a função alteraPonteiro recebe um ponteiro para um inteiro por referência. Isso significa que a função pode modificar o valor apontado pelo ponteiro original, ponteiro1, alterando o conteúdo do endereço de memória apontado por ptr.

Ao chamar a função alteraPonteiro(ponteiro1), o ponteiro ponteiro1 é passado por referência para a função, permitindo que a função modifique o valor apontado por ponteiro1.

**Explicação 3**
- A referência para um ponteiro é o endereço do ponteiro, não o endereço que o ponteiro armazena
- pode ser usado para passar o próprio podenteiro como um parâmetro para uma função 

exemplo de código 
```
#include <iostream>

using namespace std;

void incrementaPonteiro(int*& p) {
    p++; // incrementa o valor do ponteiro
}

int main() {
    int x = 10;
    int* p = &x; // ponteiro para x
    incrementaPonteiro(p); // passa uma referência para o ponteiro
    cout << *p << endl; // imprime o valor apontado pelo ponteiro (deve ser o endereço de memória de x + 4 bytes, dependendo da arquitetura)
    return 0;
}
```

#### Por que passar um ponteiro para uma função se eu posso passar a variável ?
Em alguns casos, você pode enviar diretamente a variável para a função e alterar seu valor dentro da função sem a necessidade de usar um ponteiro. No entanto, em outros casos, pode ser necessário usar um ponteiro para passar o endereço de memória da variável para a função.

Por exemplo, se você tiver uma função que precisa alterar o valor de uma variável que foi declarada em uma função externa, você precisará usar um ponteiro para acessar e modificar o valor da variável. Outro exemplo seria se você tiver uma função que precisa alocar dinamicamente uma nova memória e retornar seu endereço, você precisaria usar um ponteiro para armazenar o endereço da nova memória alocada.

O uso de um ponteiro para passar o endereço de memória da variável para uma função pode ser particularmente útil quando você deseja passar uma grande estrutura ou objeto para a função, pois passar a variável por valor pode levar a uma cópia desnecessária de todo o objeto, o que pode ser custoso em termos de tempo e memória. Ao passar um ponteiro para a função, você pode economizar recursos, pois apenas o endereço da variável é passado e não a cópia completa do objeto.

#### diferença entre "referência para um ponteiro *&" e "um ponteiro de um ponteiro **" 

Vamos supor que temos uma função chamada alteraPonteiro que recebe um ponteiro como parâmetro e modifica o valor apontado pelo ponteiro. Usando uma referência para um ponteiro *&, a função alteraPonteiro pode ser escrita da seguinte forma

```
void alteraPonteiro(int* &ptr) {
    *ptr = 10; // altera o valor apontado pelo ponteiro
}

int main() {
    int num = 5;
    int* ptr = &num; // ponteiro para a variável num
    alteraPonteiro(ptr); // chama a função alteraPonteiro
    std::cout << *ptr; // imprime 10
    return 0;
}
```

Nesse exemplo, o ponteiro ptr é inicializado com o endereço da variável num. A função alteraPonteiro recebe a referência *&ptr, o que significa que a função pode modificar o ponteiro original ptr passado para ela. Dentro da função, o valor apontado pelo ponteiro ptr é alterado para 10. Quando a função retorna, o valor de num é modificado para 10, já que o ponteiro ptr aponta para a variável num.

Em contraste, se quisermos usar um ponteiro para um ponteiro ** para passar o ponteiro para a função, poderíamos escrever o código da seguinte forma:

```
void alteraPonteiro(int** ptr) {
    **ptr = 10; // altera o valor apontado pelo ponteiro
}

int main() {
    int num = 5;
    int* ptr = &num; // ponteiro para a variável num
    alteraPonteiro(&ptr); // passa um ponteiro para o ponteiro ptr
    std::cout << *ptr; // imprime 10
    return 0;
}
```

Nesse exemplo, a função alteraPonteiro recebe um ponteiro para um ponteiro **ptr, que permite que a função modifique o ponteiro original ptr indiretamente, acessando o endereço de ptr. Dentro da função, o valor apontado pelo ponteiro *ptr é alterado para 10. Quando a função retorna, o valor de num é modificado para 10, já que o ponteiro ptr aponta para a variável num.

a principal diferença entre a referência para um ponteiro *& e um ponteiro para um ponteiro ** é que a referência é apenas outro nome para o mesmo ponteiro, enquanto o ponteiro para um ponteiro aponta para outro ponteiro. Ambos são úteis em situações diferentes, dependendo do que você precisa fazer com os ponteiros.


#### para modificar um ponteiro em uma função eu devo utilizar "ponteiro de ponteiro " ou "referência de um ponteiro"

Lembre-se que modificar existe uma diferença entre modificar o valor do ponteiro e modificar o próprio ponteiro

Quando você passa um ponteiro como argumento, a função recebe `uma cópia do valor do endereço de memória apontado pelo ponteiro`, e qualquer modificação feita na cópia não afeta o valor original do ponteiro.

Por outro lado, quando passamos um "ponteiro de ponteiro" ou "referência de um ponteiro", a função recebe o endereço de memória do ponteiro original, permitindo que ela possa modificar o valor apontado pelo ponteiro original.


#### Recapitulando
- ponteiro armazena um endereço de memória
- * acessa o valor de um ponteiro( que no caso é um endereço de memória)
  - não confundir a definição de um ponteiro `int* x` com o valor de um ponteiro `cout << *x << endl;`
- & acessa o valor da memória
- imagine a função `void dobrar(int* num)`
  - `int *num` ou `int* num` espera um argumento do tipo `int&`, ou seja, uma referência a uma variável do tipo int
- &x = tipo_do_dado *
  - &x = int*
- você só passa o endereço para um ponteiro
### Vector
- [Initialize a vector in C++](https://www.geeksforgeeks.org/initialize-a-vector-in-cpp-different-ways/)
  - `vector<int> vect;`
  - Specifying size `vector<int> vect(size, 10);`  Create a vector of size n with and all values as 10.
  - Initializing like arrays `vector<int> vect{ 10, 20, 30 };`

### Pair
- Pair is used to combine together two values that may be of different data types. Pair provides a way to store two heterogeneous objects as a single unit. It is basically used if we want to store tuples.

Example: 
```
    // defining a pair
    pair<int, char> PAIR1;
  
    // first part of the pair
    PAIR1.first = 100;
    // second part of the pair
    PAIR1.second = 'G';
  
    cout << PAIR1.first << " ";
    cout << PAIR1.second << endl;
```

- [link](https://www.geeksforgeeks.org/pair-in-cpp-stl/)


### INT_MAX and INT_MIN
- **INT_MAX** is a macro that specifies that an integer variable cannot store any value beyond this limit. 
- **INT_MIN** specifies that an integer variable cannot store any value below this limit.
- [Link](https://www.geeksforgeeks.org/int_max-int_min-cc-applications/)


### std::Sort()

- precisa importar o `algorithm`
- sua estrutura é no formato:
  - `void sort (RandomAccessIterator first, RandomAccessIterator last, Compare comp);`
- exemplo:
```
  int demo[5] = {5, 4, 3, 2, 1};
  std::sort(demo, demo + len);//Sorting demo array
```
  - Here, `demo` is the address of the first element and `(demo + len)` is the address of the last element of the given array. 
    - [link](https://www.digitalocean.com/community/tutorials/sort-in-c-plus-plus) 


### std::swap()
The function std::swap() is a built-in function in the C++ Standard Template Library (STL) which swaps the value of two variables.

- [link](https://www.geeksforgeeks.org/swap-in-cpp/)


### **#include******

#### **#include <iostream>**
- serve para utilizar as funções de IO, tipo `cout`
- um objeto importante é o [cin](https://www.geeksforgeeks.org/cin-in-c/) que serve para salvar o input do usuário(teclado) 

#### **#include <string>**
funções para string

#### **include** <cstring>
funções para string, como o **strtok**

[link](https://cplusplus.com/reference/cstring/)

### Function

#### What is the function of an asterisk before a function name?
The * refers to the return type of the function, which is void *.

When you declare a pointer variable, it is the same thing to put the * close to the variable name or the variable type:

```
int *a;
int* a;
```

- [link](https://stackoverflow.com/questions/8911230/what-is-the-function-of-an-asterisk-before-a-function-name)

### string 

#### strtok()

- The strtok() function is used in tokenizing a string based on a delimiter. It is present in the header file “string.h” and returns a pointer to the next token if present, if the next token is not present it returns NULL. To get all the tokens the idea is to call this function in a loop.

**Header File:**
`#include <string.h>`
**Syntax**
`char *strtok(char str[], const char *delims);`

#### find()

- [Link](https://www.geeksforgeeks.org/string-find-in-cpp/)

#### getline()
- [link](https://cplusplus.com/reference/string/string/getline/?kw=getline)

example
```
// extract to string
#include <iostream>
#include <string>

int main ()
{
  std::string name;

  std::cout << "Please, enter your full name: ";
  std::getline (std::cin,name);
  std::cout << "Hello, " << name << "!\n";

  return 0;
}
```


#### Tokenization
token = word
#### String tokenization - stringstream class

#### String tokenization - strok function


### new keyword

- [link 1](https://www.geeksforgeeks.org/new-vs-operator-new-in-cpp/)
- [link 2](https://www.geeksforgeeks.org/new-and-delete-operators-in-cpp-for-dynamic-memory/)

#### Normal Array Declaration vs Using new 
- [link](https://www.geeksforgeeks.org/new-and-delete-operators-in-cpp-for-dynamic-memory/)

#### em c++, qual a diferença de instanciar uma classe com "new" e sem?

Em C++, a diferença entre instanciar uma classe com "new" e sem "new" está no fato de que, ao usar "new", o objeto é criado dinamicamente no heap e o ponteiro para o objeto é retornado, enquanto sem "new", o objeto é criado automaticamente na stack.

Ao criar um objeto dinamicamente usando "new", você tem controle total sobre quando o objeto é criado e destruído, e pode liberar a memória alocada para o objeto quando não precisar mais dele, usando o operador "delete". Porém, é preciso ter cuidado ao usar "new", pois se não liberar a memória alocada, isso pode levar a vazamentos de memória.

Por outro lado, quando você cria um objeto automaticamente na stack, a memória para o objeto é alocada automaticamente e é liberada quando a função em que o objeto foi criado retorna. Isso é geralmente mais simples e seguro do que alocar memória dinamicamente usando "new", mas o tamanho do objeto é limitado ao tamanho da stack.

Em resumo, a diferença entre instanciar uma classe com "new" e sem "new" está na forma como a memória para o objeto é alocada e gerenciada. O uso de "new" é mais flexível, mas requer cuidado ao gerenciar a memória alocada, enquanto o uso de objetos criados automaticamente na stack é mais simples e seguro.

### this keyword
A palavra-chave this é útil quando você precisa acessar membros da classe dentro de uma função e há um conflito de nomes entre os parâmetros da função e os membros da classe. O this permite que você diferencie entre os dois. Por exemplo:
```
class MyClass {
public:
    void set_value(int value) {
        this->value = value;
    }
    int get_value() {
        return value;
    }
private:
    int value;
};
```

### Template

#### diferença entre "template<typename T>" e "template<class T>"
não há diferença prática entre o uso de typename e class em declarações de templates.

imagina que você esteja criando "uma família de"
[link](https://en.cppreference.com/w/cpp/language/templates)

### Generics
Example **generic function**
```
#include <iostream>
using namespace std;

// One function works for all data types.
// This would work even for user defined types
// if operator '>' is overloaded
template <typename T>

T myMax(T x, T y)
{
	return (x > y) ? x : y;
}

int main()
{

	// Call myMax for int
	cout << myMax<int>(3, 7) << endl;

	// call myMax for double
	cout << myMax<double>(3.0, 7.0) << endl;

	// call myMax for char
	cout << myMax<char>('g', 'e') << endl;

	return 0;
}

```

output
```
7
7
g
```

[link](https://www.geeksforgeeks.org/generics-in-c/)

### Class
Example
```
#include <bits/stdc++.h>
using namespace std;
class Geeks {
    // Access specifier
public:
    // Data  Members
    string geekname;
    // Member Functions()
    void printname() { cout << "Geekname is:" << geekname; }
};
int main()
{
    // Declare an object of class geeks
    Geeks obj1;
    // accessing data member
    obj1.geekname = "leandro";
    // accessing member function
    obj1.printname();
    return 0;
}
```
[link](https://www.geeksforgeeks.org/c-classes-and-objects/)


####  qual a diferença entre usar o "." e "->"

- O operador "." é usado para acessar membros de uma classe através de uma instância ou objeto da classe, enquanto o operador "->" é usado para acessar membros de uma classe através de um ponteiro para a classe.
- a diferença entre "." e "->" está no fato de que o operador "." é usado quando temos uma instância da classe, enquanto o operador "->" é usado quando temos um ponteiro para a classe.

- [link](https://stackoverflow.com/questions/3834067/c-difference-between-and-in-parameter-passing)

## Problems

### Arrays & Vectors

#### Subarray sort
Given an array of size at-least two, find the smallest subarray that needs to be sorted in place so that entire input array becomes sorted.

**Sample input**
`a1 = [1,2,3,4,5,8,6,7,9,10,11]`

**Sample output**
`[5,7]`

#### Min **Swaps**
Given an array of size N, find the minimum number of swaps needed to make the array as sorted

**Sample Input**
`a1 = [5,4,3,2,1]`

**Sample Output**
`2`

a Ideia é montar um array "espelho" e contar a troca pelos indices do array
- primeiro salva os valores do array junto com seu index
- ordena o array, neste momento os index também estão invertidos
- depois monta um array "espelho", neste caso seria `a2 = [1,2,3,4,5]` 
- a contagem das trocas se dá pela inversão do array normal com o do espelho, 
  - um exemplo seria o `5` que tem array **0** e seria trocado pelo array **4** do array espelho
- Existem casos em que o valor está correto, neste caso tanto o array quanto o array espelho estão no mesmo lugar


#### Space20

Given a string, write a function to replace all spaces in a string with '%20'. It is given that the string has sufficient space at the end to hold the additional characters.

**Sample Input**
`hello world, how are you?`

**Sample Output**
`hello%20world,%20%20how%20are%20you?`

#### Sort Strings

Given a list of 'n'strings S-,S1,S2,...,Sn-1, each consisting of digits and spaces, the number of spaces is the same for each entry, the goal is to implement a variation of a sort command. None of the strings contains consecutive spaces. Also, no string start with a space nor ends with it.
Spaces are used to divide string into coluns, which can be used as keys in comparisons.

**Sample Input**
`hello world, how are you?`

**Sample Output**
`hello%20world,%20%20how%20are%20you?`

### Sliding window problems

#### Prefix sum

### Sorting & Searching

#### D&C algorithm - Divide-and-conquer algorithm

#### merge sort

#### inversion count code 

Given an array containing integers, you need to count the total number of inversions

**inversion** two elements a[i] and a[j] form an inversion if `a[i] > a[j] and i<j`

**sample input**
`[0,5,2,3,1]`

invertions = (5,2),(5,3),(2,1),(3,1)

**sample output**
`5`


#### Quicksort

#### clrs book

#### Quickselect

Write a function that takes input an array of distinct integers, and returns kth smallest in the array - retorna o menor n menor elemento

**sample input**
`[10,5,2,0,7,6,4]
k = 4
`

**sample output**
`5`


#### smallest string

you're given a list of n strings a1,a2, ... , an.
You'd like to concatenate them together in some order such that the resulting string would be lexicographically smallest.
Give the list of strings, output the lexicographically smallest concatenation.

#### Sparse Search

Given a sorted array of string that is interspersed with empty strings, write a method to find the location of a given string.

**input**
`['ai', '', '', 'bat','','','car','cat','','','dog','']
bat
`

**output**
`4`


### Linked lists

#### Reverse a linked list

Given a linked list, write a function to reverse the linked list

**input**

1->2->3->4->5

**output**

5->4->3->2->1


#### K-reverse

Given a linked list, write a function to reverse every k nodes (where k is an input to the function)

**input**
1->2->3->4->5->6->7->8->null
k = 3 // reverse the first 3 nodes
**output**
3->2->1->6->5->4->8->7->null


#### Merge 2 sorted linked lists
Given two sorted linked list, merge them into a new linked list

**Input**
1->5->7->10->null
2->3->6->null

**output**
1->2->3->5->6->7->10->null
