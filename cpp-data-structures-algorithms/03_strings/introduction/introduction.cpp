#include <iostream>
#include <string>
using namespace std;

int main()
{
    char string_char[100] = {'1', 'a', 'b', 'c', '\0'};

    string s = "Hello world"; // is a dynamic array

    for (char ch : s)
    {
        cout << ch << ",";
    }

    cout << endl;
    string new_string;
    getline(cin, new_string, '.');

    for (char ch : new_string)
    {
        cout << ch << ",";
    }

    return 0;
}