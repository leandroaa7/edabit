#include <iostream>
using namespace std;

char *mystrtok(char *str, char delim)
{
    if (str == NULL)
    {
        return NULL;
    }

    static char *input = NULL;
    cout << "str=" << str << endl;

    input = str;

    char *new_token = new char[strlen(input) + 1];
    int i = 0;

    for (; input[i] != '\0'; i++)
    {
        bool is_delimiter = input[i] == delim;

        if (!is_delimiter)
        {
            new_token[i] = input[i];
        }
        else
        {
            new_token[i] = '\0';
            input = input + i + 1; // jump to index of next token
            return new_token;
        }
    }

    // out of the loop
    new_token[i] = '\0';

    // reset the input as NULL
    input = NULL;
    return new_token;
}

int main()
{

    char s[1000];
    cin.getline(s, 1000);

    char *token = mystrtok(s, ' ');

    while (token != NULL)
    {
        cout << token << endl;
        token = mystrtok(NULL, ' ');
    }

    return 0;
}