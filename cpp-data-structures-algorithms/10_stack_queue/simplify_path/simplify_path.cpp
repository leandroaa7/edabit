#include <iostream>
#include <stack>
#include <vector>
using namespace std;

string simplifyPath(string path)
{
    cout << path << endl;
    stack<char> stack;
    bool dotFlag = false;
    int length = path.length();
    for (int i = 0; i < length; i++)
    {
        char c = path[i];
        switch (c)
        {
        case '.':
        {
            int size = stack.size();
            if (dotFlag && size == 0)
            {
                stack.push('..');
            }
            if (dotFlag && size >= 1)
            {
                stack.pop();
            }
            dotFlag = !dotFlag;
            break;
        }
        case '/':
        {
            dotFlag = false;
            break;
        }

        default:
        {
            stack.push(c);
            dotFlag = false;
            break;
        }
        }
    }

    std::string new_path = "";
    std::vector<char> pathList;
    while (stack.size() >= 1)
    {
        pathList.push_back(stack.top());
        stack.pop();
    }

    if (pathList.size() == 0 || pathList[0] == '.')
    {
        return "/";
    }

    int firstElementIndex = pathList.size() - 1;
    char firstElementChar = pathList[firstElementIndex];
    string firstElement = std::string(1, firstElementChar);

    if (firstElement == ".")
    {
        new_path = "..";
    }
    else if (firstElement == "/")
    {
        new_path = "/";
    }
    else
    {
        new_path = "/" + firstElement;
    }
    for (int i = pathList.size() - 2; i >= 0; --i)
    {
        string element = std::string(1, pathList[i]);
        new_path += "/" + element;
    }
    return new_path;
}

int main()
{
    string s = "/a/../.././../../.";
    string path = simplifyPath(s);
    return 0;
}