#include <iostream>
#include <vector>

using namespace std;

void combinations(std::vector<int> &arr, int i, int j, int k)
{
    int max = 0;
    int newI = -1;
    int newJ = -1;
    for (int x = 0; x < k - 1; x++)
    {
        int startLeft = i;
        int endLeft = startLeft + k - x - 1;
        std::vector<int> left(arr.begin() + startLeft, arr.begin() + endLeft);

        int endRight = j + 1;
        int startRight = endRight - 1 - x;
        std::vector<int> right(arr.begin() + startRight, arr.begin() + endRight);

        std::vector<int> concat;
        concat.insert(concat.end(), left.begin(), left.end());
        concat.insert(concat.end(), right.begin(), right.end());

        int sum = 0;
        for (int num : concat)
        {
            sum += num;
        }

        if (sum > max)
        {
            max = sum;
            newI = startLeft + 1;
            newJ = startRight - 1;
        }
    }

    std::cout << "Maximum Sum: " << max << std::endl;
    std::cout << "Indices: " << newI << " " << newJ << std::endl;
}

int main()
{
    std::vector<int> arr = {1, 2, 3, 4};
    int i = 0;
    int j = arr.size() - 1;
    int k = 2;
    combinations(arr, i, j, k);
    return 0;
}