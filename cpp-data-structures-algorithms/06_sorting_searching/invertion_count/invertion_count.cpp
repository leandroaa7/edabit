#include <iostream>
#include <vector>
using namespace std;

// helper method
int merge(vector<int> &array, int s, int e)
{
    int m = (s + e) / 2;
    int i = s;
    int j = m + 1;

    vector<int> temp;

    int cnt = 0;

    while (i <= m and j <= e)
    {
        if (array[i] < array[j])
        {
            temp.push_back(array[i]);
            i++;
        }
        else
        {
            cnt += (m - i + 1);
            temp.push_back(array[j]);
            j++;
        }
    }

    // copy rem element from first array
    while (i <= m)
    {
        temp.push_back(array[i]);
        i++;
    }

    // copy rem element from second array
    while (j <= e)
    {
        temp.push_back(array[j]);
        j++;
    }

    // copy back the elements from temp to original array
    int k = 0;
    for (int idx = s; idx <= e; idx++)
    {
        array[idx] = temp[k];
        k++;
    }
    return cnt;
}

// sorting method
int inversion_count(vector<int> &arr, int s, int e)
{
    // base case
    if (s >= e)
    {
        return 0;
    }

    // recursive case
    int mid = (s + e) / 2;
    int c1 = inversion_count(arr, s, mid);
    int c2 = inversion_count(arr, mid + 1, e);
    int ci = merge(arr, s, e);

    return c1 + c2 + c1;
}

int main()
{
    vector<int> arr{0, 5, 2, 3, 1};
    int s = 0;
    int e = arr.size() - 1;
    cout << inversion_count(arr, s, e) << endl;
    cout << "fim" << endl;
    return 0;
}