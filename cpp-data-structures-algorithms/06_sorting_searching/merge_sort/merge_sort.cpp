#include <iostream>
#include <vector>
using namespace std;

// helper method
void merge(vector<int> &array, int s, int e)
{
    cout << "..::new iteration::.." << endl;
    int mid = (s + e) / 2;
    int i = s;
    int j = mid + 1;

    vector<int> temp;

    /*
        vai colocando no temp os item ordenadamente
        isso comparando o elemento do:
             index_inicio com o index_depois_do_meio
        a comparação acaba quando OU
            o index_início chega no meio
            o index_depois_do_meio chega no final
     */
    while (i <= mid and j <= e)
    {
        int element_i = array[i];
        int element_j = array[j];

        cout << "element_i=" << element_i << endl;
        cout << "element_j=" << element_j << endl;

        if (element_i < element_j)
        {
            temp.push_back(element_i);
            i++;
        }
        else
        {
            temp.push_back(element_j);
            j++;
        }
    }

    // copy remaining elements from first array
    while (i <= mid)
    {
        temp.push_back(array[i]);
        i++;
    }
    // or copy remaining element from second array
    while (j <= e)
    {
        temp.push_back(array[j]);
        j++;
    }

    // copy back the elements from temp to original array
    int k = 0;
    for (int idx = s; idx <= e; idx++)
    {
        array[idx] = temp[k++];
    }
    return;
}

// sorting method
/*
s=start
e=end
*/
void mergesort(vector<int> &array, int start, int end)
{
    // base case
    if (start >= end)
    {
        return;
    }

    // recursive case
    int mid = (start + end) / 2;
    mergesort(array, start, mid);
    mergesort(array, (mid + 1), end);
    return merge(array, start, end);
}

int main()
{
    vector<int> array{10, 5, 2, 0, 7, 6, 4};

    int s = 0;
    int e = array.size() - 1;
    mergesort(array, s, e);
    for (int x : array)
    {
        cout << x << ",";
    }
    return 0;
}