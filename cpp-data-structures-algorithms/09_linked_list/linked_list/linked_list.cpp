#include <iostream>
using namespace std;

class node
{
public:
    int data;
    node *next;

    node(int data)
    {
        this->data = data;
        next = NULL;
    }
};

void insertAtHead(node *&head, int data)
{
    bool is_first_node = head == NULL;
    if (is_first_node)
    {
        head = new node(data);
        return;
    }

    node *n = new node(data);
    n->next = head;
    head = n; // o head recebe um novo endereço
}

void printLL(node *head)
{
    int data;
    while (head != NULL)
    {
        data = head->data;
        cout << data << "-->";
        head = head->next;
    }
}

int main()
{
    node *head = NULL; // a cabeça fica na frente, logo a última cabeça fica no começo
    insertAtHead(head, 4);
    insertAtHead(head, 3);
    insertAtHead(head, 1);
    printLL(head);
    return 0;
}
