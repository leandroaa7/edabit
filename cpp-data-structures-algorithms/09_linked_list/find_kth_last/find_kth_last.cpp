#include <stdio.h>
#include <iostream>

using namespace std;

class node
{
public:
    int data;
    node *next;

    node(int data)
    {
        this->data = data;
        next = NULL;
    }
};

int kthLastElement(node *head, int k)
{
    node *fastPointer = head;
    node *slowPointer = head;

    for (int i = 0; i < k - 1; i++)
    {
        fastPointer = fastPointer->next;
    }

    while (fastPointer->next != NULL)
    {
        fastPointer = fastPointer->next;
        slowPointer = slowPointer->next;
    }

    return slowPointer->data;
}

int main()
{

    node *head = new node(1);
    node *temp = head;
    for (int i = 2; i <= 7; i++)
    {
        node *n = new node(i);
        temp->next = n;
        temp = n;
    }
    int kElement = kthLastElement(head, 3);
    cout << kElement << endl;
    return 0;
}