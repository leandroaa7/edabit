#include <iostream>

using namespace std;

using namespace std;

class node
{
public:
    int data;
    node *next;

    node(int data)
    {
        this->data = data;
        next = NULL;
    }
};

int getMid(node *head)
{
    node *current = head;

    while (current != NULL)
    {
        cout << current->data << endl;
        current = current->next;
    }

    node *hase = head->next;
    node *tortoise = head;
    while (hase != NULL && hase->next != NULL)
    {
        tortoise = tortoise->next;
        hase = hase->next->next;
    }
    return tortoise->data;
}

int main()
{

    node *head = new node(1);
    node *temp = head;
    for (int i = 2; i <= 8; i++)
    {
        node *n = new node(i);
        temp->next = n;
        temp = n;
    }
    int mid = getMid(head);
    cout << mid << endl;
    return 0;
}