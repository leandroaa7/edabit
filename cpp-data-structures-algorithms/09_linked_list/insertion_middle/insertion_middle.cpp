#include <iostream>

using namespace std;

class node
{
public:
    int data;
    node *next;

    node(int data)
    {
        this->data = data;
        next = NULL;
    }
};

void insertAtHead(node *&head, int data)
{
    bool is_first_node = head == NULL;
    if (is_first_node)
    {
        head = new node(data);
        return;
    }

    node *n = new node(data);
    n->next = head;
    head = n; // o head recebe um novo endereço
}

void insertInMiddle(node *&head, int data, int pos)
{
    if (pos == 0)
    {
        insertAtHead(head, data);
    }

    node *temp = head;
    int next_index_after_first_element = 1;

    // vai até o penúltimo elemento
    for (int jump = next_index_after_first_element; jump <= pos - 1; jump++)
    {
        temp = temp->next;
    }

    node *new_node = new node(data);
    new_node->next = temp->next;
    temp->next = new_node;
}

void printLL(node *head)
{
    int data;
    while (head != NULL)
    {
        data = head->data;
        cout << data << "-->";
        head = head->next;
    }
}

int main()
{
    node *head = NULL;
    insertAtHead(head, 4);
    insertAtHead(head, 3);
    insertAtHead(head, 5);
    insertInMiddle(head, 8, 2);
    printLL(head);

    return 0;
}