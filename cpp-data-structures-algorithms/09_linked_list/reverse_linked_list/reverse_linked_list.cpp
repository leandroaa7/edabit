#include <iostream>

using namespace std;

class node
{

public:
    int data;
    node *next;
    node(int data)
    {
        this->data = data;
        next = NULL;
    }
};

void insertAtHead(node *&head, int data)
{
    bool is_first_node = head == NULL;
    if (is_first_node)
    {
        head = new node(data);
        return;
    }

    node *new_node = new node(data);
    new_node->next = head;
    head = new_node;
}

void insertInMiddle(node *&head, int data, int pos)
{
    if (pos == 0)
    {
        insertAtHead(head, data);
        return;
    }

    node *temp = head;
    int next_index_after_first_element = 1;
    int index_of_element_before_pos = pos - 1;
    for (int jump = next_index_after_first_element; jump <= index_of_element_before_pos; jump++)
    {
        temp = temp->next;
    }

    node *new_node = new node(data);
    new_node->next = temp->next;
    temp->next = new_node;
}

void reverse(node *&head)
{
    node *prev = NULL;
    node *current = head;
    node *temp = NULL;
    while (current != NULL)
    {
        // store next
        temp = current->next;
        // update the current
        current->next = prev;

        // prev and current
        prev = current;
        current = temp;
    }
    head = prev;
    return;
}

node *recReverse(node *head)
{
    // base case
    if (head == NULL or head->next == NULL)
    {
        return head;
    }
    // otherwise
    node *sHead = recReverse(head->next);
    head->next->next = head;
    head->next = NULL;
    return sHead;
}

void printLL(node *head)
{
    int data;
    while (head != NULL)
    {
        data = head->data;
        cout << data << "->";
        head = head->next;
    }
}

int main()
{
    node *head = NULL;
    insertAtHead(head, 3);
    insertAtHead(head, 2);
    /*     insertAtHead(head, 7);
        insertAtHead(head, 8);
        insertInMiddle(head, 10, 1); */
    printLL(head);
    head = recReverse(head);
    cout << "\n"
         << endl;
    printLL(head);
    cout << "\n"
         << endl;
    reverse(head);
    printLL(head);
    return 0;
}