#include <vector>

using namespace std;

int recFindPath(int m, int n, vector<vector<int>> v, int r, int c)
{
    if (r == m - 1 && c == n - 1)
    {
        return 1;
    }
    if (r > m || c > n)
    {
        return 0;
    }

    int right = 0;
    int down = 0;
    int up = 0;
    int left = 0;
    const bool isValidNextRow = r + 1 < m;
    const bool isValidNextColumn = c + 1 < n;
    const bool isValidPreviousRow = r - 1 >= 0;
    const bool isValidPreviousColumn = c - 1 >= 0;

    /* right */
    if (isValidNextColumn && v[r][c + 1] == 1)
    {
        v[r][c] = 0;
        right = 1 + recFindPath(m, n, v, r, c + 1);
        v[r][c] = 1;
    }

    /* down */
    if (isValidNextRow && v[r + 1][c] == 1)
    {
        v[r][c] = 0;
        down = 1 + recFindPath(m, n, v, r + 1, c);
        v[r][c] = 1;
    }
    /* up */
    if (isValidPreviousRow && v[r - 1][c] == 1)
    {
        v[r][c] = 0;
        up = 1 + recFindPath(m, n, v, r - 1, c);
        v[r][c] = 1;
    }
    /* left */
    if (isValidPreviousColumn && v[r][c - 1] == 1)
    {
        v[r][c] = 0;
        left = 1 + recFindPath(m, n, v, r, c - 1);
        v[r][c] = 1;
    }

    int max1 = std::max(right,down);
    int max2 = std::max(up,down);
    int maxFinal = std::max(max1,max2);
    return maxFinal;
}

int findLongestPath(int m, int n, vector<vector<int>> v)
{
    int result = recFindPath(m, n, v, 0, 0) - 1;
    return result;
}

int main()
{
    vector<vector<int>> v = {
        {1, 1, 1},
        {1, 1, 1},
        {0, 0, 1},
    };
    int m = 3;
    int n = 3;
    return findLongestPath(m, n, v);
}