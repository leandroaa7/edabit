#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int maxValueMultiple(std::vector<int> &arr, int i, int j, int k, int sum)
{
    const int coins = j - i + 1;
    /* i + k == j */
    if (j == i + 1 || k >= coins)
    {
        int newSum = 0;
        for (int x = i; x <= j; x++)
        {
            newSum += arr[x];
        }
        return newSum;
    }

    /* esquerda */
    int newSumLeft = 0;
    for (int x = i; x < i + k; x++)
    {
        newSumLeft += arr[x];
    }
    const int left = sum - maxValueMultiple(arr, i + k, j, k, sum - newSumLeft);

    /* direita */
    int newSumRight = 0;
    for (int x = j; x > j - k; x--)
    {
        newSumRight += arr[x];
    }
    const int right = sum - maxValueMultiple(arr, i, j - k, k, sum - newSumRight);
    /* mista */

    int newSumMixed = 0;
    int newI = -1;
    int newJ = -1;
    for (int x = 0; x < k - 1; x++)
    {
        int startLeft = i;
        int endLeft = startLeft + k - x - 1;
        std::vector<int> left(arr.begin() + startLeft, arr.begin() + endLeft);

        int endRight = j + 1;
        int startRight = endRight - 1 - x;
        std::vector<int> right(arr.begin() + startRight, arr.begin() + endRight);

        std::vector<int> concat;
        concat.insert(concat.end(), left.begin(), left.end());
        concat.insert(concat.end(), right.begin(), right.end());

        int new_sum = 0;
        for (int num : concat)
        {
            new_sum += num;
        }

        if (new_sum > newSumMixed)
        {
            newSumMixed = new_sum;
            newI = startLeft + 1;
            newJ = startRight - 1;
        }
    }

    const int mixed = sum - maxValueMultiple(arr, newI, newJ, k, sum - newSumMixed);
    int max1 = std::max(left, right);
    int max2 = std::max(max1, mixed);
    return max2;
}

int maxValueSingle(std::vector<int> &arr, int i, int j, int sum)
{
    const int hasOnlyTwoCoins = i + 1 == j;
    if (hasOnlyTwoCoins)
    {
        // when have two options only
        return std::max(arr[i], arr[j]);
    }

    /* For both of your choises, the opponent
    gives you total sum minus maximum of his value */
    const int indexCasePlayerChooseLeft = i + 1;
    const int indexCasePlayerChooseRight = j - 1;
    const int sumCaseChooseLeft = sum - arr[i];
    const int sumCaseChooseRight = sum - arr[j];
    const int leftChoise = sum - maxValueSingle(arr, indexCasePlayerChooseLeft, j, sumCaseChooseLeft);
    const int rightChoise = sum - maxValueSingle(arr, i, indexCasePlayerChooseRight, sumCaseChooseRight);
    return std::max(leftChoise, rightChoise);
}

int maxValue(int n, vector<int> &v, int k)
{
    int sum = 0;
    for (int i = 0; i < n; i++)
    {
        sum += v[i];
    }
    if (k == 1)
    {
        return maxValueSingle(v, 0, n - 1, sum);
    }
    return maxValueMultiple(v, 0, n - 1, k, sum);
}

int main()
{
    int k = 1;
    vector<int> v = {10, 15, 20, 9, 2};
    const int n = v.size();

    cout << maxValue(n, v, k) << endl;

    return 0;
}
