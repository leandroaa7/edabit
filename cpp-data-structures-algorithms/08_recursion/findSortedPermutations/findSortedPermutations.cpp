#include <iostream>
#include <set>
#include <string>
#include <vector>

using namespace std;

void generatePermutations(int i, string &sol, const string &a, set<string> &permutations)
{
    int nSol = sol.size();
    int nA = a.size();
    if (nSol == nA)
    {
        permutations.insert(sol);
    }
    else
    {
        for (int k = 0; k < nA; ++k)
        {
            auto element = a[k];
            string partial = sol.substr(0, i);

            if (partial.find(element) == string::npos)
            {
                sol.push_back(element);
                generatePermutations(i + 1, sol, a, permutations);
                sol.pop_back(); // Remove o último caractere para backtracking
            }
        }
    }
}

void generatePermutations2(int i, string &sol, const string &original, set<string> &permutations, vector<bool> &used)
{
    int size = original.size();
    if (i == size)
    {
        permutations.insert(sol);
        return;
    }

    for (int k = 0; k < original.size(); ++k)
    {
        if (!used[k])
        {
            sol.push_back(original[k]);
            used[k] = true;
            generatePermutations2(i + 1, sol, original, permutations, used);
            sol.pop_back();
            used[k] = false;
        }
    }
}

vector<string> sortedPermutations(const string &S)
{
    set<string> permutations;
    string sol = "";
    vector<bool> used(S.size(), false);
    generatePermutations2(0, sol, S, permutations, used);

    // Convertendo set para vector
    vector<string> result(permutations.begin(), permutations.end());

    return result;
}

int main()
{
    set<string> permutations;
    string sol = "";
    string a = "acaa";
    int i = 0;

    generatePermutations(i, sol, a, permutations);
    vector<string> myVector(permutations.begin(), permutations.end());

    string input = "acaa";
    vector<string> output = sortedPermutations(input);

    // Output as permutações ordenadas
    for (const string& permutation : output) {
        cout << permutation << endl;
    }
    return 0;
}
