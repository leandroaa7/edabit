#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

vector<vector<int>> triplets(vector<int> arr, int target_sum)
{
    int arr_size = arr.size();
    sort(arr.begin(), arr.end());
    vector<vector<int>> result;

    // Pick every a[i], pair sum for remaining part
    for (int i = 0; i < arr_size - 3; i++)
    {
        int index_to_begin_arr = i + 1;
        int index_to_end_arr = arr_size - 1;

        // two pointer approach
        while (index_to_begin_arr < index_to_end_arr)
        {
            int current_sum = arr[i];
            current_sum += arr[index_to_begin_arr];
            current_sum += arr[index_to_end_arr];

            bool found_triplet = current_sum == target_sum;

            if (found_triplet)
            {
                result.push_back({arr[i], arr[index_to_begin_arr], arr[index_to_end_arr]});
                index_to_begin_arr++;
                index_to_end_arr--;
            }
            else if (current_sum > target_sum)
            {
                index_to_end_arr--;
            }
            else
            {
                index_to_begin_arr++;
            }
        }
    }

    return result;
}

int main()
{
    vector<int> arr{1, 2, 3, 4, 5, 6, 7, 8, 9, 15};
    int S = 18;
    auto triplets_vector = triplets(arr, S);

    for (auto triplet : triplets_vector)
    {
        for (auto number : triplet)
        {
            cout << number << ",";
        }
        cout << endl;
    }

    return 0;
}
