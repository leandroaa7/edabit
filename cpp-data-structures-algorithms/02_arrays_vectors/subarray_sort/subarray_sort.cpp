#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

bool outOfOrder(vector<int> arr, int i)
{
    int element = arr[i];

    bool isFirstElementOfList = i == 0;
    if (isFirstElementOfList)
    {
        bool isGreaterThanNextElement = element > arr[1];
        return isGreaterThanNextElement;
    }

    int previousElement = arr[i - 1];
    int nextElement = arr[i + 1];
    bool isLastElementOfList = i == arr.size() - 1;
    if (isLastElementOfList)
    {
        bool isLessThanPreviousElement = element < previousElement;
        return isLessThanPreviousElement;
    }
    bool isGreaterThanNextElement = element > nextElement;
    bool isLessThanPreviousElement = element < previousElement;
    return isGreaterThanNextElement or isLessThanPreviousElement;
}

pair<int, int> subarraySort(vector<int> arr)
{
    int smallest = INT_MAX; // this will be checked with the small element found
    int largest = INT_MIN;  // this will be checked with the larger element found

    for (int i = 0; i < arr.size(); i++)
    {
        int element = arr[i];
        bool isOutOfOrder = outOfOrder(arr, i);
        if (isOutOfOrder)
        {
            smallest = min(smallest, element);
            largest = max(largest, element);
        }
    }

    // next step find the right index where smallest
    // and largest lie (subarray) for out solution
    if (smallest == INT_MAX)
    {
        return {-1, -1};
    }

    int left = 0; // index to put smallest
    while (smallest >= arr[left])
    {
        left++;
    }

    int right = arr.size() - 1; // index to put largest
    while (largest <= arr[right])
    {
        right--;
    }
    return {left, right};
}

int main()
{
    vector<int> arr = {1, 2, 3, 4, 5, 8, 6, 7, 9, 10, 11};
    auto p = subarraySort(arr);
    cout << p.first << " and " << p.second << endl;
    return 0;
}