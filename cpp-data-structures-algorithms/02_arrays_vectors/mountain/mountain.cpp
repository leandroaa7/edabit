#include <iostream>
#include <vector>
using namespace std;

int highest_mountain(vector<int> arr)
{
    int n = arr.size();
    int largest = 0;
    // first and last element can't be a peak
    for (int i = 1; i <= n - 2;)
    {
        int current_number = arr[i];
        int previous_number = arr[i - 1];
        int next_number = arr[i + 1];
        // bool isPeak =
        if (current_number > previous_number and current_number > next_number)
        {
            int count = 1;
            int index_to_count_previous_numbers = i;
            int current_element_to_count = arr[index_to_count_previous_numbers];
            int previous_element_to_count = arr[index_to_count_previous_numbers - 1];
            // count backwards (left)
            while (index_to_count_previous_numbers >= 1 and (current_element_to_count > previous_element_to_count)) // bool isNotZero = index_to_count_previous_numbers >= 0; boold isGreaterThanPrevious
            {
                index_to_count_previous_numbers--;
                count++;
                current_element_to_count = arr[index_to_count_previous_numbers];
                if (index_to_count_previous_numbers >= 1)
                {
                    previous_element_to_count = arr[index_to_count_previous_numbers - 1];
                }
            }

            // count forwards (right)
            current_element_to_count = arr[i];
            int next_element_to_count = arr[i + 1];

            while (i <= n - 2 and current_element_to_count > next_element_to_count)
            {
                i++;
                count++;
                current_element_to_count = arr[i];
                next_element_to_count = arr[i + 1];
            }
            largest = max(largest, count);
        }
        else
        {
            i++; // for update condition
        }
    }
    return largest;
}

int main()
{

    vector<int> arr{5, 6, 1, 2, 3, 4, 5, 4, 3, 2, 0, 1, 2, 3, -2, 4};
    cout << highest_mountain(arr);

    return 0;
}