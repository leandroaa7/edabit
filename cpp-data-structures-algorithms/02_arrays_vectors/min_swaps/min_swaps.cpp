#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int counMinSwaps(vector<int> arr)
{
    int n = arr.size();
    // Know the actual positions of elements (sorting)
    // store the current indices
    pair<int, int> ap[n]; // array of pairs
    for (int i = 0; i < n; i++)
    {
        ap[i].first = arr[i];
        ap[i].second = i;
    }

    // sorting pairs
    sort(ap, ap + n);

    // build the main logic
    // array to know if element was visited
    vector<bool> visited(n, false);

    int ans = 0;

    for (int i = 0; i < n; i++)
    {
        // if element is visited or element is in right position
        int old_index_position = ap[i].second;
        bool is_in_right_position = old_index_position == i;
        bool is_visited = visited[i] == true;
        if (is_visited or is_in_right_position)
        {
            continue;
        }
        // visiting the element (index) for first time
        int index_node = i;
        int cycle = 0;

        while (!visited[index_node])
        {
            visited[index_node] = true;
            int next_index_node = ap[index_node].second;
            index_node = next_index_node;
            cycle += 1;
        }
        ans += (cycle - 1);
    }
    return ans;
}

int main()
{
    vector<int> arr{5, 4, 3, 2, 1};
    cout << counMinSwaps(arr) << endl;
    arr = {2, 4, 5, 1, 3};
    cout << counMinSwaps(arr) << endl;

    return 0;
}