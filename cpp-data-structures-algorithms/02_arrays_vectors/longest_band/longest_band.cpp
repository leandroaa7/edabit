#include <iostream>
#include <vector>
#include <unordered_set>
using namespace std;

int largestBand(vector<int> arr)
{
    unordered_set<int> s;

    // data inside a set
    for (int x : arr)
    {
        s.insert(x);
    }

    // Iterate over the arr
    int largest_len = 1;

    for (auto element : s)
    {
        int parent = element - 1;

        // if has no parent element, therefore is the first element of a band
        bool has_no_parent_element = s.find(parent) == s.end();

        if (has_no_parent_element)
        {
            // find entire band / chain starting from element
            int next_element = element + 1;
            int count = 1;
            bool has_next_element = s.find(next_element) != s.end();

            while (has_next_element)
            {
                next_element++;
                count++;
            }
            if (count > largest_len)
            {
                largest_len = count;
            }
        }
    }

    return largest_len;
}

int main()
{
    vector<int> arr{1, 9, 3, 0, 18, 5, 2, 4, 10, 7, 12, 6};
    cout << largestBand(arr);
    return 0;
}