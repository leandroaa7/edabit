#include <iostream>
#include <vector>
using namespace std;

int trappedWater(vector<int> heights)
{
    int n = heights.size();
    bool has_enough_elements = n <= 2;
    if (has_enough_elements)
    {
        return 0;
    }

    // left MAX, right MAX
    vector<int> left(n, 0);
    vector<int> right(n, 0);

    left[0] = heights[0];          // it starts to the first of array
    right[n - 1] = heights[n - 1]; // it starts to the last of array
    int index_right_elements = n - 1;
    for (int i = 1; i < n; i++)
    {
        index_right_elements = n - i - 1;
        left[i] = max(left[i - 1], heights[i]);
        right[index_right_elements] = max(right[n - i], heights[index_right_elements]);
    }

    // water
    int water = 0;
    int min_water = 0;
    for (int i = 0; i < n; i++)
    {
        min_water = min(left[i], right[i]) - heights[i];
        water += min_water;
    }
    return water;
}

int main()
{
    vector<int> water = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
    cout << trappedWater(water) << endl;
    return 0;
}