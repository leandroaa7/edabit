/* Given two strings, write a function to determine if the
second string is an anagram of the first. An anagram is a word,
phrase, or name formed by rearranging the letters of another, such as cinema,
formed from iceman. */

function validAnagram(first, second) {
  if (first.length !== second.length) {
    return false;
  }

  const lookup = {};

  for (const element of first) {
    let letter = element;
    // if letter exist, increment, otherwise set to 1
    lookup[letter] = lookup[letter] ? lookup[letter] + 1 : 1;
  }

  for (const element of second) {
    let letter = element;
    // can't find letter or letter is zero then it's not an anagram
    if (!lookup[letter]) {
      return false;
    } else {
      lookup[letter]--;
    }
  }
  return true;
}

console.log(validAnagram("ana", "naa"));
