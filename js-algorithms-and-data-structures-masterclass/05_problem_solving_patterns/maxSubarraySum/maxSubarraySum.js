/* 
Write a function called maxSubarraySum
which accepts an array of integers and a number called N.
The function should calculate the maximum sum of N consecutive
elements in the array */
let maxSum;
maxSum = maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 2); // 10
maxSum = maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 4); // 17
maxSum = maxSubarraySum([4, 2, 1, 6], 1); // 6
maxSum = maxSubarraySum([4, 2, 1, 6, 2], 4); // 13
maxSum = maxSubarraySum([], 4); // null

function maxSubarraySum(arr, num) {
  let maxSum = 0;
  let tempSum = 0;
  if (arr.length < num) {
    return null;
  }
  for (let i = 0; i < num; i++) {
    maxSum += arr[i];
  }
  tempSum = maxSum;
  for (let i = num; i < arr.length; i++) {
    const previousElement = arr[i - num];
    const nextElement = arr[i];
    tempSum = tempSum - previousElement + nextElement;
    maxSum = Math.max(maxSum, tempSum);
  }
  return maxSum;
}
