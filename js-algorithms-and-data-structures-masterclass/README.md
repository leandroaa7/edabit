[js-algorithms-and-data-structures-masterclass](https://www.udemy.com/course/js-algorithms-and-data-structures-masterclass/)


## Problem solving patterns

### Frequency counter

This pattern uses object or sets to collect values/frequencies of 
values.

This can often avoid the need for nested loops or O(N^2) operations with arrays / strings

#### Problems 
- anagram

### Multiple pointers

Creating pointers or values that correspond to an index or position and move towards the beginning, end or middle based on a certain condition 

Very efficient for solving problems with minimal space complexity as well

![two_pointers](./docs/imgs/two_pointers.png)
#### Problems
- countUniqueValues


### Sliding Window

This pattern involves creating a window which can either be an array or number from one position to another

Depending on a certain condition, the window either increases or closes (and a new window is created)

Very useful for keepng track of subset of data in an array/string etc.

![sliding_window](./docs/imgs/sliding_window.webp)

<hr>

![sliding_window](./docs/imgs/sliding_window_max_sum.png)

#### Problems
- maxSubarraySum


### Divide and Conquer Pattern
This pattern involves dividing a data set into smaller chunks and then repeating a process with a subset of data.

This pattern can tremendously decrease time complexity.


## Recursion 

### What is recursion?

A process (a function in our case) that calls itself

##